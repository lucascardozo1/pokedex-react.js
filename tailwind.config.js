/** @type {import("tailwindcss").Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    colors: require("./colors.config.js"),
    extend: []
  },
  plugins: [
    require("@tailwindcss/forms")
  ]
}