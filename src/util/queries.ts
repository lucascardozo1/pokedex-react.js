import { type DocumentNode, gql } from '@apollo/client'
import { type PokemonFilter } from '../types'

export const GET_POKEMON_DETAILS = gql`
    query GetPokemonDetails($id: Int) {
        pokemon: pokemon_v2_pokemon(where: {id: {_eq: $id}}) {
            id
            name
            height
            weight
            types: pokemon_v2_pokemontypes {
                type: pokemon_v2_type {
                    name
                    id
                }
            }
            specy: pokemon_v2_pokemonspecy {
                id
                name
                color: pokemon_v2_pokemoncolor {
                    id
                    name
                }
                evolution_chain: pokemon_v2_evolutionchain {
                    species: pokemon_v2_pokemonspecies {
                        id
                        name
                        evolutions: pokemon_v2_pokemons {
                            id
                            name
                            images: pokemon_v2_pokemonsprites {
                                sprites
                            }
                        }
                    }
                }
            }
            moves: pokemon_v2_pokemonmoves(distinct_on: move_id) {
                move: pokemon_v2_move {
                    id
                    name
                }
            }
            images: pokemon_v2_pokemonsprites {
                sprites
            }
        }
    }
`

export const GET_ALL_TYPES = gql`
    query GetAllPokemonTypes {
        pokemon_types: pokemon_v2_type {
            name
            id
        }
    }
`

export const GET_ALL_COLORS = gql`
    query GetAllPokemonColors {
        pokemon_colors: pokemon_v2_pokemoncolor {
            id
            name
        }
    }
`

export const GET_FILTERED_POKEMONS = ({ name, isBaby, colorId, typeIds, weight }: PokemonFilter): DocumentNode => {
  const whereClause = `,
    {
      name: { ${name.trim().length > 2 ? `_ilike: "%${name}%"` : ''} },
      _or: { weight: { _gte: ${weight[0] ?? 0} ${weight[1] !== undefined ? `,_lte: ${weight[1]}` : ''} } },
      pokemon_v2_pokemonspecy: {
        is_baby: { ${isBaby !== undefined ? `_eq: ${isBaby}` : ''} },
        pokemon_v2_pokemoncolor: {
          id: { ${colorId !== undefined ? `_eq: ${colorId}` : ''} }
        }
      },
      pokemon_v2_pokemontypes: {
        pokemon_v2_type: {
          id: { ${typeIds.length !== 0 ? `_in: [${typeIds.toString()}]` : ''} }
        }
      }
    }
  `

  return gql`
        query GetFilteredPokemons ($offset: Int = 0, $limit: Int = 10) {
            pokemons: pokemon_v2_pokemon(limit: $limit, offset: $offset, where: ${whereClause}) {
                id
                name
                types: pokemon_v2_pokemontypes {
                    type: pokemon_v2_type {
                        name
                        id
                    }
                }
                weight
                specy: pokemon_v2_pokemonspecy {
                    is_baby
                    color: pokemon_v2_pokemoncolor {
                        id
                        name
                    }
                }
                images: pokemon_v2_pokemonsprites {
                    sprites
                }
            }
        }
    `
}
