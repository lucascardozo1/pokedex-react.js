export const toCapitalCase = (str: string): string => str.charAt(0).toUpperCase() + str.substring(1)
