import { type ApolloError, useQuery } from '@apollo/client'
import { GET_POKEMON_DETAILS } from '../util/queries.ts'
import { type GetPokemonDetailsResult, type PokemonDetails } from '../types'

interface HookReturn {
  pokemon: PokemonDetails | undefined
  loading: boolean
  error: ApolloError | undefined
}

function usePokemonDetails (id: string): HookReturn {
  const { loading, error, data } = useQuery<GetPokemonDetailsResult>(GET_POKEMON_DETAILS, {
    variables: {
      id
    }
  })

  let pokemon: PokemonDetails | undefined

  if (data !== undefined && data.pokemon.length !== 0) {
    const species = JSON.parse(JSON.stringify(data.pokemon[0].specy.evolution_chain.species))

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    species.forEach(species => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      species.evolutions = species.evolutions.filter(evolution => evolution.id === species.id)
    })

    pokemon = {
      id: data.pokemon[0].id,
      name: data.pokemon[0].name,
      color: data.pokemon[0].specy.color,
      height: data.pokemon[0].height,
      weight: data.pokemon[0].weight,
      moves: data.pokemon[0].moves.map(v => v.move),
      sprites: JSON.parse(data.pokemon[0].images[0].sprites),
      types: data.pokemon[0].types.map(v => v.type),
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      evolutions: species.flatMap(v => v.evolutions).map(v => {
        return {
          ...v,
          // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
          sprites: JSON.parse(v.images[0].sprites)
        }
      })
    }

    console.log(pokemon)
  }

  return { loading, error, pokemon }
}

export default usePokemonDetails
