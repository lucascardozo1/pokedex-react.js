import { useEffect, useRef, useState } from 'react'

interface Props {
  distance: number
}

interface HookReturn<T extends Element> {
  nearScreen: boolean
  fromRef: React.RefObject<T>
}

function useNearScreen<T extends Element> ({ distance = 100 }: Props): HookReturn<T> {
  const [nearScreen, setNearScreen] = useState(false)
  const fromRef = useRef<T>(null)

  useEffect(() => {
    const onChange = (entries: IntersectionObserverEntry[]): void => {
      const element = entries[0]
      if (element.isIntersecting) {
        setNearScreen(true)
      }
    }

    const observer = new IntersectionObserver(onChange, {
      rootMargin: distance + 'px'
    })

    if (fromRef.current === null) throw new Error('error')

    observer.observe(fromRef.current)

    return () => { observer.disconnect() }
  })

  return { nearScreen, fromRef }
}

export default useNearScreen
