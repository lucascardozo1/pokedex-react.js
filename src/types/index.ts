type SrcOrNull = string | null

export interface PokemonSprites {
  front_default: SrcOrNull
  front_female: SrcOrNull
  front_shiny: SrcOrNull
  front_shiny_female: SrcOrNull
  back_default: SrcOrNull
  back_female: SrcOrNull
  back_shiny: SrcOrNull
  back_shiny_female: SrcOrNull
  other: {
    dream_world: {
      front_default: SrcOrNull
      front_female: SrcOrNull
    }
    home: {
      front_default: SrcOrNull
      front_female: SrcOrNull
      front_shiny: SrcOrNull
      front_shiny_female: SrcOrNull
    }
    'official-artwork': {
      front_default: SrcOrNull
      front_shiny: SrcOrNull
    }
  }
  versions: {
    'generation-i': {
      'red-blue': {
        front_default: SrcOrNull
        front_gray: SrcOrNull
        back_default: SrcOrNull
        back_gray: SrcOrNull
        front_transparent: SrcOrNull
        back_transparent: SrcOrNull
      }
      yellow: {
        front_default: SrcOrNull
        front_gray: SrcOrNull
        back_default: SrcOrNull
        back_gray: SrcOrNull
        front_transparent: SrcOrNull
        back_transparent: SrcOrNull
      }
    }
    'generation-ii': {
      crystal: {
        front_default: SrcOrNull
        front_shiny: SrcOrNull
        back_default: SrcOrNull
        back_shiny: SrcOrNull
        front_transparent: SrcOrNull
        front_shiny_transparent: SrcOrNull
        back_transparent: SrcOrNull
        back_shiny_transparent: SrcOrNull
      }
      gold: {
        front_default: SrcOrNull
        front_shiny: SrcOrNull
        back_default: SrcOrNull
        back_shiny: SrcOrNull
        front_transparent: SrcOrNull
      }
      silver: {
        front_default: SrcOrNull
        front_shiny: SrcOrNull
        back_default: SrcOrNull
        back_shiny: SrcOrNull
        front_transparent: SrcOrNull
      }
    }
    'generation-iii': {
      emerald: {
        front_default: SrcOrNull
        front_shiny: SrcOrNull
      }
      'firered-leafgreen': {
        front_default: SrcOrNull
        front_shiny: SrcOrNull
        back_default: SrcOrNull
        back_shiny: SrcOrNull
      }
      'ruby-sapphire': {
        front_default: SrcOrNull
        front_shiny: SrcOrNull
        back_default: SrcOrNull
        back_shiny: SrcOrNull
      }
    }
    'generation-iv': {
      'diamond-pearl': {
        front_default: SrcOrNull
        front_female: SrcOrNull
        front_shiny: SrcOrNull
        front_shiny_female: SrcOrNull
        back_default: SrcOrNull
        back_female: SrcOrNull
        back_shiny: SrcOrNull
        back_shiny_female: SrcOrNull
      }
      'heartgold-soulsilver': {
        front_default: SrcOrNull
        front_female: SrcOrNull
        front_shiny: SrcOrNull
        front_shiny_female: SrcOrNull
        back_default: SrcOrNull
        back_female: SrcOrNull
        back_shiny: SrcOrNull
        back_shiny_female: SrcOrNull
      }
      platinum: {
        front_default: SrcOrNull
        front_female: SrcOrNull
        front_shiny: SrcOrNull
        front_shiny_female: SrcOrNull
        back_default: SrcOrNull
        back_female: SrcOrNull
        back_shiny: SrcOrNull
        back_shiny_female: SrcOrNull
      }
    }
    'generation-v': {
      'black-white': {
        front_default: SrcOrNull
        front_female: SrcOrNull
        front_shiny: SrcOrNull
        front_shiny_female: SrcOrNull
        back_default: SrcOrNull
        back_female: SrcOrNull
        back_shiny: SrcOrNull
        back_shiny_female: SrcOrNull
        animated: {
          front_default: SrcOrNull
          front_female: SrcOrNull
          front_shiny: SrcOrNull
          front_shiny_female: SrcOrNull
          back_default: SrcOrNull
          back_female: SrcOrNull
          back_shiny: SrcOrNull
          back_shiny_female: SrcOrNull
        }
      }
    }
    'generation-vi': {
      'omegaruby-alphasapphire': {
        front_default: SrcOrNull
        front_female: SrcOrNull
        front_shiny: SrcOrNull
        front_shiny_female: SrcOrNull
      }
      'x-y': {
        front_default: SrcOrNull
        front_female: SrcOrNull
        front_shiny: SrcOrNull
        front_shiny_female: SrcOrNull
      }
    }
    'generation-vii': {
      'ultra-sun-ultra-moon': {
        front_default: SrcOrNull
        front_female: SrcOrNull
        front_shiny: SrcOrNull
        front_shiny_female: SrcOrNull
      }
      icons: {
        front_default: SrcOrNull
        front_female: SrcOrNull
      }
    }
    'generation-viii': {
      icons: {
        front_default: SrcOrNull
        front_female: SrcOrNull
      }
    }
  }
}

export interface PokemonType {
  id: number
  name: string
}

export interface Pokemon {
  id: number
  name: string
  types: PokemonType[]
  sprites: PokemonSprites
}

export interface PokemonDetails extends Pokemon {
  weight: number
  height: number
  color: PokemonColor
  evolutions: Evolution[]
  moves: PokemonMove[]
}

export type Evolution = Pick<Pokemon, 'id' | 'name' | 'sprites'>

export interface PokemonFilter {
  name: string
  isBaby?: boolean
  colorId?: number
  weight: Array<number | undefined>
  typeIds: number[]
}

export interface OffsetPagination {
  limit: number
  offset: number
}

export interface PokemonColor {
  name: string
  id: number
}

export interface PokemonMove {
  id: number
  name: string
}

export interface PokemonResult {
  id: number
  name: string
  types: Array<{
    type: PokemonType
  }>
  weight: number
  specy: {
    is_baby: boolean
    color: PokemonColor
  }
  images: Array<{
    sprites: string
  }>
}

export interface GetPokemonDetailsResult {
  pokemon: PokemonResultExtended[]
}

export interface GetFilteredPokemonsResult {
  pokemons: PokemonResult[]
}

export interface GetAllTypesResult {
  pokemon_types: PokemonType[]
}

export interface GetAllColorsResult {
  pokemon_colors: PokemonColor[]
}

interface PokemonResultExtended {
  id: number
  name: string
  height: number
  weight: number
  types: Array<{
    type: {
      name: string
      id: number
    }
  }>
  specy: {
    id: number
    name: string
    color: {
      id: number
      name: string
    }
    evolution_chain: {
      species: Array<{
        id: number
        name: string
        evolutions: Array<{
          id: number
          name: string
          images: Array<{
            sprites: string
          }>
        }>
      }>
    }
  }
  moves: Array<{
    move: {
      id: number
      name: string
    }
  }>
  images: Array<{
    sprites: string
  }>
}
