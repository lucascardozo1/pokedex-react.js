import './App.css'
import PokemonSearch from './components/PokemonSearch.tsx'
import { Link, Route, Routes, useParams } from 'react-router-dom'
import PokemonDetails from './components/PokemonDetails.tsx'

function App (): JSX.Element {
  return (
      <div className="p-4">
        <div className="flex justify-between items-center mb-4">
          <img src="/pokemon-logo.svg" alt=""/>
          <nav>
            <ul>
              <li>
                <Link to="/about" className="text-xl">About</Link>
              </li>
            </ul>
          </nav>
        </div>
        <Routes>
          <Route path="/" element={<PokemonSearch/>}/>
          <Route path="/about" element={<h1>About</h1>}/>
          <Route path="/pokemon/:id" element={<PokemonDetails />}/>
        </Routes>
        <div>
          <h6 className="text-center text-xl">Design and implementation by Lucas Cardozo</h6>
        </div>
      </div>
  )
}

export default App
