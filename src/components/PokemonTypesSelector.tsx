import { useQuery } from '@apollo/client'
import { GET_ALL_TYPES } from '../util/queries.ts'
import { toCapitalCase } from '../util/utils.ts'
import { type GetAllTypesResult } from '../types'

interface Props {
  checkedIds: number[]
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

function PokemonTypesSelector ({ checkedIds, onChange }: Props): React.JSX.Element {
  const { loading, error, data } = useQuery<GetAllTypesResult>(GET_ALL_TYPES)

  if (loading) return <p>Loading...</p>
  if (error !== undefined) return <p>Error : {error.message}</p>

  return (
      <ul>
        {data?.pokemon_types.map(type => <li key={type.id} className="flex justify-between">
          <label htmlFor={`${type.name}_checkbox`}>{toCapitalCase(type.name)}</label>
          <input type="checkbox" checked={checkedIds.includes(type.id)} value={type.id} onChange={onChange} />
        </li>)}
      </ul>
  )
}

export default PokemonTypesSelector
