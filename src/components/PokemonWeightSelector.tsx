interface Props {
  weight: Array<number | undefined>
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

function PokemonWeightSelector ({ weight, onChange }: Props): JSX.Element {
  return (
      <>
        <div className="flex gap-4">
            <input className="nice-input" type="number" name="wMin" placeholder="From..." min={0} step={0.1} value={weight[0] ?? ''}
                   onChange={onChange}/>
            <input className="nice-input" type="number" name="wMax" placeholder="To..." min={0} step={0.1} value={weight[1] ?? ''}
                   onChange={onChange}/>
        </div>
      </>
  )
}

export default PokemonWeightSelector
