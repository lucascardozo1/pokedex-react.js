import { useParams } from 'react-router-dom'
import PokemonTypes from './PokemonTypes.tsx'
import PokemonEvolutions from './PokemonEvolutions.tsx'
import usePokemonDetails from '../hooks/usePokemonDetails.ts'
import Loader from './Loader.tsx'
import { toCapitalCase } from '../util/utils.ts'

function PokemonDetails (): JSX.Element {
  const { id } = useParams()
  if (id === undefined) return <div>Error</div>

  const { pokemon, loading, error } = usePokemonDetails(id)

  if (pokemon === undefined) return <div>Error</div>

  return (loading
    ? <Loader />
    : <div>
        <div className="lg:flex gap-4">
          <div className="flex flex-col justify-center">
            <img src={pokemon.sprites.other.home.front_default ?? '/pokeball.png'} alt={pokemon?.name}/>
          </div>
          <div className="flex flex-1 flex-col gap-4">
            <h1 className="text-5xl">{toCapitalCase(pokemon.name)}</h1>
            <div>
              <div>
                <h2 className="text-3xl inline-block">Weight:</h2>
                <p className="text-4xl inline-block">{pokemon.weight}</p>
              </div>
              <div>
                <h2 className="text-3xl inline-block">Height:</h2>
                <span className="text-4xl inline-block">{pokemon.height}</span>
              </div>
            </div>
            <div>
              <h2 className="text-3xl">Types</h2>
              <PokemonTypes types={pokemon.types}/>
            </div>
            <div>
              <h2 className="text-3xl">Moves</h2>
              <ul className="flex flex-wrap gap-2">
                {pokemon.moves.map(move => <li key={move.name} className="rounded-full bg-[#fff] p-2">
                  <span className="text-lg">{move.name.replace('-', ' ')}</span>
                </li>)}
              </ul>
            </div>
          </div>
        </div>
        <div className="mt-24">
          <h2 className="text-center text-3xl">Evolution Chain</h2>
          <PokemonEvolutions evolutions={pokemon.evolutions} />
        </div>
      </div>
  )
}

export default PokemonDetails
