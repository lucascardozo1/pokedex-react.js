import { type PokemonFilter } from '../types'
import PokemonTypesSelector from './PokemonTypesSelector.tsx'
import PokemonColorSelector from './PokemonColorSelector.tsx'
import PokemonWeightSelector from './PokemonWeightSelector.tsx'
import { useState } from 'react'

interface Props {
  filter: PokemonFilter
  handlePokemonNameChange: (event: React.ChangeEvent<HTMLInputElement>) => void
  handlePokemonIsBabyChange: (event: React.ChangeEvent<HTMLSelectElement>) => void
  handlePokemonColorChange: (event: React.ChangeEvent<HTMLSelectElement>) => void
  handlePokemonTypeChange: (event: React.ChangeEvent<HTMLInputElement>) => void
  handlePokemonWeightChange: (event: React.ChangeEvent<HTMLInputElement>) => void
  handleFilterReset: () => void
}

function FilterContainer ({
  filter,
  handlePokemonIsBabyChange,
  handlePokemonColorChange,
  handlePokemonTypeChange,
  handlePokemonNameChange,
  handlePokemonWeightChange,
  handleFilterReset
}: Props): JSX.Element {
  const [showFilter, setShowFilter] = useState(true)

  return (
      <div className="p-4">
        <button className="block lg:hidden" onClick={() => {
          setShowFilter(!showFilter)
        }}>{showFilter ? 'Hide' : 'Show'} filter
        </button>
        <form action="#" className={showFilter ? 'flex flex-col gap-4' : 'hidden'}>
          <fieldset>
            <legend className="text-xl">Name</legend>
            <input className="nice-input" type="text" value={filter.name} placeholder="Type at least 3 characters..."
                   onChange={handlePokemonNameChange}/>
          </fieldset>
          <fieldset>
            <legend className="text-xl">Is baby?</legend>
            <select className="nice-input" name="isBaby" onChange={handlePokemonIsBabyChange} value={filter.isBaby?.toString() ?? '-1'}>
              <option value="-1">Nothing selected</option>
              <option value="false">No</option>
              <option value="true">Yes</option>
            </select>
          </fieldset>
          <fieldset>
            <legend className="text-xl">Color</legend>
            <PokemonColorSelector selectedId={filter.colorId} onChange={handlePokemonColorChange}/>
          </fieldset>
          <fieldset className="flex flex-col">
            <legend className="text-xl">Weight</legend>
            <PokemonWeightSelector weight={filter.weight} onChange={handlePokemonWeightChange}/>
          </fieldset>
          <fieldset>
            <legend className="text-xl">Type(s)</legend>
            <PokemonTypesSelector checkedIds={filter.typeIds} onChange={handlePokemonTypeChange}/>
          </fieldset>
          <button type="button" onClick={handleFilterReset}>Reset filter</button>
        </form>
      </div>
  )
}

export default FilterContainer
