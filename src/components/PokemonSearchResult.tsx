import { type GetFilteredPokemonsResult, type PokemonFilter } from '../types'
import { useQuery } from '@apollo/client'
import { GET_FILTERED_POKEMONS } from '../util/queries.ts'
import Loader from './Loader.tsx'
import PokemonCards from './PokemonCards.tsx'
import { useState } from 'react'

interface Props {
  filter: PokemonFilter
}

function PokemonSearchResult ({ filter }: Props): JSX.Element {
  const { loading, error, data, fetchMore } = useQuery<GetFilteredPokemonsResult>(GET_FILTERED_POKEMONS(filter), {
    variables: {
      limit: 10,
      offset: 0
    }
  })

  function fetchMoreData (): void {
    console.log(data?.pokemons.length)
    void fetchMore({
      variables: {
        offset: data?.pokemons.length
      },
      updateQuery: (prevResults, { fetchMoreResult }): GetFilteredPokemonsResult => {
        console.log(prevResults)
        console.log(fetchMoreResult)

        return {
          pokemons: [...prevResults.pokemons, ...fetchMoreResult.pokemons
          ]
        }
      }
    })
  }

  return (
      <div>
        {loading
          ? <Loader/>
          : <PokemonCards data={data === undefined ? [] : data.pokemons} fetchMoreData={fetchMoreData}/>}
      </div>
  )
}

export default PokemonSearchResult
