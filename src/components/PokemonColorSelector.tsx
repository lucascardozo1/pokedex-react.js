import { useQuery } from '@apollo/client'
import { GET_ALL_COLORS } from '../util/queries.ts'
import { toCapitalCase } from '../util/utils.ts'
import { type GetAllColorsResult } from '../types'

interface Props {
  selectedId?: number
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void
}

function PokemonColorSelector ({ selectedId, onChange }: Props): React.JSX.Element {
  const { loading, error, data } = useQuery<GetAllColorsResult>(GET_ALL_COLORS)

  if (error !== undefined) return <p>Error : {error.message}</p>
  let elements: JSX.Element
  if (loading) {
    elements = <option>Loading...</option>
  } else {
    elements = (
        <>
          <option value="-1">No color selected</option>
          {data?.pokemon_colors
            .map(color => <option key={color.id} value={color.id}>{toCapitalCase(color.name)}</option>)}
        </>
    )
  }

  return (
        <select className="nice-input" name="color" onChange={onChange} value={selectedId ?? '-1'}>
          {elements}
        </select>
  )
}

export default PokemonColorSelector
