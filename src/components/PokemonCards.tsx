import PokemonCard from './PokemonCard.tsx'
import { type PokemonResult } from '../types'
import InfiniteScroll from 'react-infinite-scroll-component'
import Loader from './Loader.tsx'

interface Props {
  data: PokemonResult[]
  fetchMoreData: () => void
}

function PokemonCards ({ data, fetchMoreData }: Props): JSX.Element {
  const pokemons = data.map(data => {
    return {
      id: data.id,
      name: data.name,
      types: data.types.map(type => type.type),
      sprites: JSON.parse(data.images[0].sprites)
    }
  })

  const items = pokemons.map((pokemon) => <PokemonCard pokemon={pokemon} key={pokemon.id}/>)

  return (
      <>
        <InfiniteScroll next={fetchMoreData} hasMore={data.length !== 0} endMessage={<h1>No more results</h1>} loader={<Loader/>} dataLength={items.length}>
          {items}
        </InfiniteScroll>
      </>
  )
}

export default PokemonCards
