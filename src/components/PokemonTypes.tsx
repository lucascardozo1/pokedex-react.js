import { type PokemonType } from '../types'
import { toCapitalCase } from '../util/utils.ts'

function PokemonTypes ({ types }: { types: PokemonType[] }): JSX.Element {
  return (
      <ul className="flex gap-2 m-2">
        {types.map(type => <li key={type.id} className={`bg-type-${type.name} py-2 px-4 rounded-full text-pk-font-light text-xl`}>{toCapitalCase(type.name)}</li>)}
      </ul>
  )
}

export default PokemonTypes
