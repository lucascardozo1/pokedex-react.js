import { type Pokemon } from '../types'
import { toCapitalCase } from '../util/utils.ts'
import PokemonTypes from './PokemonTypes.tsx'
import { Link } from 'react-router-dom'

function PokemonCard ({ pokemon }: { pokemon: Pokemon }): JSX.Element {
  return (
      <div className="rounded-md p-2 bg-card shadow-2xl flex flex-col justify-items-start items-center h-fit">
        <img src={pokemon.sprites.other.home.front_default ?? '/pokeball.png'} alt={pokemon.name}/>
        <div className="bg-gray-400 h-[1px] w-full m-2"/>
        <h5 className="text-center text-pk-font-black text-5xl md:text-4xl">{toCapitalCase(pokemon.name)}</h5>
        <PokemonTypes types={pokemon.types} />
        <button className="bg-[#1c8796] text-[#4affbc] rounded-full text-xl px-8 py-1 my-3"><Link to={`/pokemon/${pokemon.id}`}>Go to details</Link></button>
        <div className="bg-pk-type-poison"></div>
      </div>
  )
}

export default PokemonCard
