import Filter from './FilterContainer.tsx'
import PokemonSearchResult from './PokemonSearchResult.tsx'
import { useState } from 'react'
import type { PokemonFilter } from '../types'

function PokemonSearch (): JSX.Element {
  const [filter, setFilter] = useState<PokemonFilter>({
    name: '',
    isBaby: undefined,
    colorId: undefined,
    weight: [undefined, undefined],
    typeIds: []
  })

  function handlePokemonNameChange (e: React.ChangeEvent<HTMLInputElement>): void {
    setFilter({
      ...filter,
      name: e.target.value
    })
  }

  function handlePokemonIsBabyChange (e: React.ChangeEvent<HTMLSelectElement>): void {
    setFilter({
      ...filter,
      isBaby: e.target.value === '-1' ? undefined : e.target.value === 'true'
    })
  }

  function handlePokemonColorChange (e: React.ChangeEvent<HTMLSelectElement>): void {
    setFilter({
      ...filter,
      colorId: e.target.value === '-1' ? undefined : Number(e.target.value)
    })
  }

  function handlePokemonTypeChange (e: React.ChangeEvent<HTMLInputElement>): void {
    const prevTypeIds = filter.typeIds
    const typeId = Number(e.target.value)
    const typeIdx = prevTypeIds.indexOf(typeId)

    setFilter({
      ...filter,
      typeIds: typeIdx !== -1 ? [...prevTypeIds.slice(0, typeIdx), ...prevTypeIds.slice(typeIdx + 1)] : [...prevTypeIds, typeId]
    })
  }

  function handleFilterReset (): void {
    setFilter({
      name: '',
      isBaby: undefined,
      colorId: undefined,
      weight: [undefined, undefined],
      typeIds: []
    })
  }

  function handlePokemonWeightChange (e: React.ChangeEvent<HTMLInputElement>): void {
    const inputName = e.target.name
    const inputValue = Number(e.target.value)
    const newWeight = [...filter.weight]

    if (inputName === 'wMin') newWeight[0] = inputValue
    if (inputName === 'wMax') newWeight[1] = inputValue

    setFilter({
      ...filter,
      weight: newWeight
    })
  }

  return (
      <div className="lg:flex">
        <Filter filter={filter}
                handlePokemonNameChange={handlePokemonNameChange}
                handlePokemonIsBabyChange={handlePokemonIsBabyChange}
                handlePokemonColorChange={handlePokemonColorChange}
                handlePokemonTypeChange={handlePokemonTypeChange}
                handlePokemonWeightChange={handlePokemonWeightChange}
                handleFilterReset={handleFilterReset}/>
        <PokemonSearchResult filter={filter}/>
      </div>
  )
}

export default PokemonSearch
