function Loader (): JSX.Element {
  return <>
    <img src="/loader.gif" alt="loading"/>
    <h2 className="text-center text-2xl">Loading...</h2>
  </>
}

export default Loader
