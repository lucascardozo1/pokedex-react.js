import { type Evolution } from '../types'
import { toCapitalCase } from '../util/utils.ts'

interface Props {
  evolutions: Evolution[]
}

function PokemonEvolutions ({ evolutions }: Props): JSX.Element {
  return (
      <ul className="lg:flex justify-evenly items-center">
        {evolutions.map(evolution => {
          return (
              <li key={evolution.name} className="flex flex-col justify-center items-center">
                <img className="w-1/2" src={evolution.sprites.other.home.front_default ?? '/pokeball.png'} alt={evolution.name}/>
                <p className="text-center text-2xl">{toCapitalCase(evolution.name)}</p>
              </li>
          )
        })}
      </ul>
  )
}

export default PokemonEvolutions
